/*

  Dominik Leon Bieczyński
  Leon Instruments
  https://extronic.pl

  Biblioteka pobrana ze strony:
  https://extronic.pl/content/70-biblioteka-portx

*/

#include "extrino_portx.h"

volatile struct PORTX_t PORTX;

void PortxInit(void)
{
	// sygna�y SS dla wszystkich peryfer�w na eXtrino XL
	PORTA.OUTSET = PIN3_bm | PIN4_bm;			// SPI MEM, OP AMP
	PORTA.DIRSET = PIN3_bm | PIN4_bm;			// SPI MEM, OP AMP
	PORTE.OUTSET = PIN3_bm | PIN6_bm | PIN7_bm; // SD, PORTX, DIGPOT
	PORTE.DIRSET = PIN3_bm | PIN6_bm | PIN7_bm; // SD, PORTX, DIGPOT

	PORTC.DIRSET = PIN4_bm | PIN5_bm | PIN7_bm;
	PORTC.DIRCLR = PIN6_bm;
	PORTC.OUTCLR = PIN7_bm | PIN6_bm | PIN5_bm | PIN4_bm;
	PORTC.REMAP = PORT_SPI_bm; // zamiana miejscami SCK i MOSI
	SPIC.CTRL = SPI_ENABLE_bm |
				SPI_MASTER_bm |
				SPI_MODE_3_gc |
				//SPI_CLK2X_bm|
				SPI_PRESCALER_DIV128_gc;

#if PORTX_AUTOREFRESH
	// przerwania
	SPIC.INTCTRL = SPI_INTLVL_LO_gc;
	PMIC.CTRL = PMIC_HILVLEN_bm |  // w��czenie przerwa� o priorytecie HI
				PMIC_MEDLVLEN_bm | // w��czenie przerwa� o priorytecie MED
				PMIC_LOLVLEN_bm;   // w��czenie przerwa� o priorytecie LO
	sei();
	SPIC.DATA = 0; // pierwsza transmisja, zerowanie
#else
	PortxRefresh();
#endif
}

#if PORTX_AUTOREFRESH == 0
void PortxRefresh(void)
{
	PORTE.OUTCLR = PIN6_bm;

	//for(uint8_t i = 255; i; i--);								// op�nienie
	PORTX.OUT |= PORTX.OUTSET;
	PORTX.OUT &= ~PORTX.OUTCLR;
	PORTX.OUT ^= PORTX.OUTTGL;
	PORTX.OUTSET = 0;
	PORTX.OUTCLR = 0;
	PORTX.OUTTGL = 0;

	SPIC.DATA = PORTX.OUT;
	while (SPIC.STATUS == 0)
		;
	PORTX.IN = SPIC.DATA;
	for (uint8_t i = 255; i; i--)
		; // op�nienie
	PORTE.OUTSET = PIN6_bm;
}
#endif

ISR(SPIC_INT_vect)
{
	PORTE.OUTSET = PIN6_bm; // chip deselect
	PORTX.OUT |= PORTX.OUTSET;
	PORTX.OUT &= ~PORTX.OUTCLR;
	PORTX.OUT ^= PORTX.OUTTGL;
	PORTX.OUTSET = 0;
	PORTX.OUTCLR = 0;
	PORTX.OUTTGL = 0;
	PORTE.OUTCLR = PIN6_bm; // chip select
	PORTX.IN = SPIC.DATA;
	SPIC.DATA = PORTX.OUT;
}