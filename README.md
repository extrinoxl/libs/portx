# eXtrinoXL PORTX
PORTX to wirtualny układ peryferyjny na płytkach prototypowych eXtrino XL. Jest on jak najbardziej rzeczywisty i funkcjonalny, a słowo wirtualny oznacza to, że dzięki odpowiednim sztuczkom programistycznym, działa on zupełnie tak, jak zwyczajne standardowe porty w mikrokontrolerach XMEGA, dzięki czemu nawet początkujący wirtuoz programowania poradzi sobie z PORTX w jeden wieczór.

## Platformio IO

Bibliotekę można użyć bezpośrednio w projektach utworzonych za pomocą **Platformio IO**. Wystarczy sklonować repozytorium do katalogu **lib/** projektu a następnie dołączyć ją do kodu:

```
#include <extrino_portx.h>
```

## Źródła
* [Kurs na stronie Leon Instruments](https://extronic.pl/content/70-biblioteka-portx)

## Autorzy
* Dominik Bieczyński (Leon Instruments) - <https://extronic.pl>
* Paweł 'felixd' Wojciechowski - <http://www.konopnickiej.com> - Utworzenie repozytorium