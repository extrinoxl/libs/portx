/*

  Dominik Leon Bieczyński
  Leon Instruments
  https://extronic.pl

  Biblioteka pobrana ze strony:
  https://extronic.pl/content/70-biblioteka-portx

*/

#define F_CPU 2000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <extrino_portx.h> // Platformio IO

int main(void)
{

	PortxInit(); // inicjalizacja PORTX

	while (1)
	{
		PORTX.OUT = PORTX.IN;
	}
}
