/*

  Dominik Leon Bieczyński
  Leon Instruments
  https://extronic.pl

  Biblioteka pobrana ze strony:
  https://extronic.pl/content/70-biblioteka-portx

*/

#ifndef EXTRINO_PORTX_H_
#define EXTRINO_PORTX_H_

#include <avr/io.h>
#include <avr/interrupt.h>

// AUTOMATYCZNE ODŚWIEŻANIE PORTU X
// PORTX należy odświeżać ręcznie, gdy SPI jest wykorzystywane jeszcze do innych celów
// 1 - włączone
// 0 - wyłączone
#define PORTX_AUTOREFRESH 1

struct PORTX_t
{
	uint8_t volatile IN;
	uint8_t volatile OUT;
	uint8_t volatile OUTSET;
	uint8_t volatile OUTCLR;
	uint8_t volatile OUTTGL;
};
volatile struct PORTX_t PORTX;

// inicjalizacja PORTX
void PortxInit(void);

// odświeżenie rejestrów PORTX
#if PORTX_AUTOREFRESH == 0
void PortxRefresh(void);
#endif

#endif /* EXTRINO_PORTX_H_ */